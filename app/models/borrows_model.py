from mysql.connector import connect

class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                              database='perpustakaan',
                              user='root',
                              password='admin')
        except Exception as e :
            print(e)
    
    def showBorrowByEmail(self,**params):
        query = '''
        select customers.username, borrows.*
        from borrows 
        inner join customers on borrows.userid = customers.userid
        where customers.email = "{0}" and borrows.isactive = 1;
        '''.format(params["email"])
        cursor = self.db.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return result
        

    def insertBorrow(self,**params):
        column = ', '.join(list(params.keys()))
        values = tuple(list(params.values()))
        query = '''
        insert into borrows ({0})
        values {1};
        '''.format(column,values)
        print(query)
        cursor = self.db.cursor()
        cursor.execute(query)
        

    def updateBorrow(self,**params):
        borrowid = params["borrowid"]
        values = self.restructureParams(**params)
        query = '''
        update borrows
        set isactive = 0
        where borrowid = {0};
        '''.format(borrowid)
        cursor = self.db.cursor()
        cursor.execute(query)
        

    def dataCommit(self):
        self.db.commit()

    def restructureParams(self, **data):
        list_data = ['{0} = "{1}"'.format(item[0],item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result